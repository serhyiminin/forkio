import gulp from "gulp";
import dartSass from "sass";
import gulpSass from "gulp-sass";
const sass = gulpSass(dartSass);
import clean from "gulp-clean";
import cleanCSS from "gulp-clean-css";
import concat from "gulp-concat";
import concatCss from "gulp-concat-css";
import autoprefixer from "gulp-autoprefixer";
import imagemin from "gulp-imagemin";
import minify from "gulp-minify"; //substitute for gulp-uglify
import BS from "browser-sync";
const browserSync = BS.create();

//FUNCTIONS
const cleanDist = () => gulp.src("dist/*", { read: false }).pipe(clean()); //очистка папки dist

function buildStyles() {
  return gulp
    .src("src/scss/**/*.scss")
    .pipe(sass.sync().on("error", sass.logError))
    .pipe(concatCss("styles.min.css"))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(autoprefixer({ cascade: true }))
    .pipe(gulp.dest("dist/css"));
}

function trackCSS() {
  return gulp
    .src("./src/scss/**/*.scss")
    .pipe(sass.sync().on("error", sass.logError))
    .pipe(concatCss("styles.min.css"))
    .pipe(gulp.dest("dist/css"));
}
function trackJS() {
  return gulp
    .src("./src/js/**/*.js")
    .pipe(concat("scripts.min.js"))
    .pipe(gulp.dest("dist/js"));
}

const buildJs = () =>
  gulp
    .src("./src/js/*.js")
    .pipe(concat("scripts.min.js"))
    .pipe(
      minify({
        ext: {
          src: ".js",
          min: ".js",
        },
        // noSource: true,
      })
    )
    .pipe(gulp.dest("dist/js"));

const minifyImages = () =>
  gulp.src("./src/img/**/*").pipe(imagemin()).pipe(gulp.dest("dist/img"));

const startWatching = () => {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });
  gulp
    .watch("./src/**/*")
    .on("change", gulp.series(trackCSS, trackJS, browserSync.reload));
};

const assetsCopy = () =>
  gulp.src("./src/assets/**/*").pipe(gulp.dest("dist/assets"));

//SUBTASKS
gulp.task("cleanDist", cleanDist);
gulp.task("assetsCopy", assetsCopy);
gulp.task("minifyImages", minifyImages);
gulp.task("buildCss", buildStyles);
gulp.task("buildJs", buildJs);

//TASKS

gulp.task(
  "build",
  gulp.series("cleanDist", "assetsCopy", "minifyImages", "buildCss", "buildJs")
);

gulp.task("dev", startWatching);
